<?php

namespace App\Http\Controllers\News;

use Illuminate\Http\Request;

use Carbon\Carbon;
use Validator;
use App\Http\Requests;
use App\Http\Controllers\Controller;
use App\Models\News;

class NewsController extends Controller
{
    //-----------------
    //Validation rules
    //-----------------
    public function rule()
    {
        $rules = [];  
        switch(\Request::method())
        {
            //GET
            case 'GET' :
              $rules = [
                    'page' => 'integer|min:1',
                    'title' => 'string',
                    'draft' => 'boolean',
                    'star'  => 'boolean',
                    'confirm' => 'boolean',
                    'created_by' => 'integer|exists:account',
                    'tags' => 'array',
                    'tags.*' => 'integer|exists:tag,id',
                    'columns' => 'array',
                    'columns.*' => 'in:id,title,content,draft,created_by,created_at,updated_at'
                ];
                break;
                
            //POST
            case 'POST':
                $rules = [
                    'title' => 'required|string',
                    'content' => 'required|string|min:10',
                    'draft' => 'required|boolean',
                    'star' =>  'required|boolean',
                    'picture' => 'required|string', //base64 picture string
                    'publish_at' => 'required|date_format:Y-m-d G:i:s|after:yesterday', //date with specific format
                    'tags' => 'array',
                    'tags.*' => 'integer|exists:tag,id',
                    'slug' => 'required|unique:news,slug', //create from title
                ];
                break;
            
           //PUT
            case 'PUT':
                $rules = [
                    'title' => 'string',
                    'content' => 'string|min:10',
                    'draft' => 'boolean',
                    'star' =>  'boolean',
                    'picture' => 'string', //base64 picture string
                    'publish_at' => 'date_format:Y-m-d G:i:s', //date with specific format
                    'tags' => 'array',
                    'tags.*' => 'integer|exists:tag,id',
                    'slug' => 'unique:news,slug,'.$news->id, //create from title - except the same slug
                ];
                break;
            
            default: break;
        }
        
        return $rules;
    }

    // validate upload base64 picture for news
    public function validatePicture($base64File)
    {
        //picture special validation
        $picture = \Image::make($base64File);
        if(!$picture)
            return response()->json(['error' => true, 'data' => null, 'messages' => [ \Lang::get('news.badBase64') ] ]);
            
        if($picture->filesize() > 2097152) //size in byte - 2mb for the moment 
            return response()->json(['error' => true, 'data' => null, 'messages' => [ \Lang::get('news.tooHeavy') ]  ]);
            
        return $picture;
    }
    
    //---------------------------
    //API
    //---------------------------

    /**
    *   @api {get} /api/news/ get all news (can be filter)
    *   @apiName allNews
    *   @apiGroup News
    *   @apiParam {Number} page=1 wanted page
    *   @apiParam {Boolean} draft=0 define show/hide draft
    *   @apiParam {Number} created_by filter by created_by
    *   @apiParam {Number[]} tags filter by array of tags
    *   @apiParam {String} title filter for lookup word in title
    *   @apiParam {Boolean} star filter for lookup star
    *   @apiParam {Boolean} confirm filter for lookup confirm only
    *   @apiParam {String[]} columns specific wanted column
    */
    public function all(Request $request) 
    {
        //@TODO create middleware for auth
        
        $validator = Validator::make($request->all(), $this->rule());
        if($validator->fails()) 
            return response()->json(['error' => true, 'data' => null, 'messages' => $validator->messages()]);

        //disable for agone x)
        $news = News::with('tags');
        /*
        $news = News::with('tags')->where('draft', '=', false); //default we didn't display draft - only display when you are log as author
        $news = $news->where('publish_at', '<=', Carbon::now()); //default we didn't display news not yet publish - only display when you are log as author
        $news = $news->where('confirm', '=', true); //defaut we didn't display news not yet confirm - only display when you are log as author
        */
        
        //input
        $draft = $request->input('draft');
        $title = $request->input('title');
        $createdBy = $request->input('created_by');
        $tags = $request->input('tags');
        $page = $request->input('page');
        $confirm = $request->input('confirm');
        $star = $request->input('start');
        $columns = $request->input('columns');

        //filters
        if($draft !== null) $news = $news->where('draft', '=', $draft); //only when author
        if($createdBy !== null) $news = $news->where('created_by', '=', $createdBy);
        if($title !== null) $news = $news->where('title', 'like', '%'.$title.'%');
        if($star !== null) $news = $news->where('star', '=', $star);
        if($confirm !== null) $news = $news->where('confirm', '=', $confirm); //only when author
        if($columns !== null) $news = $news->select($columns);
        if($tags !== null) $news = $news->with('tags')->whereIn('id', $tags);

        //pagination
        $news = $news->paginate(15);
        if($page) $news->current_page = $page;

        return response()->json(['error' => false, 'data' => $news,  'messages' => null]);
    }

    /**
    *   @api {get} /api/news/:id get news item by id
    *   @apiName indexNews
    *   @apiGroup News
    *   @apiParam {Number} id wanted news
    */
    public function index($id) 
    {
        $news = News::find($id);
        return response()->json(['error' => ($news) ? false : true, 'data' => $news, 'messages' => [ ($news) ? null : \Lang::get('news.notFound')] ]);
    }
    
   /**
    *   @api {get} /api/news/:slug get news item by slug
    *   @apiName indexNewsBySlug
    *   @apiGroup News
    *   @apiParam {String} slug wanted news with slug
    */
    public function indexBySlug($slug)
    {
        $news = News::where('slug', '=', $slug)->first();
        return response()->json(['error' => ($news) ? false : true, 'data' => $news, 'messages' => [  ($news) ? null :\Lang::get('news.notFound')] ]);
    }

    /**
    *   @api {post} /api/news/ Create a news
    *   @apiName storeNews
    *   @apiGroup News
    *   @apiParam {String} title wanted news title
    *   @apiParam {String} content news content HTML available
    *   @apiParam {Boolean} draft news not publish (draft)
    *   @apiParam {Boolean} star new is star
    *   @apiParam {String} picture base64 picture encoding
    *   @apiParam {DateTime} publish_at Y-m-d G:i:s for publish date
    *   @apiParam {Number[]} tags news tags
    */
    public function store(Request $request) 
    {
        //@TODO - create middleware for auth
        $request->merge(['slug'  => str_slug($request->input('title'), '-')]); //append slug for validation

        $validator = Validator::make($request->all(), $this->rule());

        if($validator->fails()) 
            return response()->json(['error' => true, 'data' => null, 'messages' => $validator->messages()]);
        
        //validation picture
        //check if size <= 2MB
        //check if base64 is valid
        $resultPicture = $this->validatePicture($request->input('picture'));
        if($resultPicture instanceof JsonResponse)
            return $resultPicture;

        //create the news
        $news = new News;
        $news->title = $request->input('title');
        $news->slug = $request->input('slug');
        $news->content  = $request->input('content');
        $news->draft = $request->input('draft');
        $news->star = $request->input('star');
        $news->confirm = false; //not confirm when create
        $news->publish_at = Carbon::createFromFormat('Y-m-d G:i:s', $request->input('publish_at'));

        //try to save picture
        //if fail - news not save and return an error
        try 
        {
           $resultPicture->save(public_path().DIRECTORY_SEPARATOR.$news->picture, 100);
        } 
        catch (\Intervention\Image\Exception\NotWritableException $e) 
        {
            return response()->json(['error' => false, 'data' => null, 'messages' => [ \Lang::get('news.cantSavePicture')] ]);
        }

        $news->save(); //nothing fail we can save news
        
        //now news have id we can save
        //tags relationship
        if($request->input('tags'))  $news->tags()->sync($request->input('tags'));
        $news->load('tags');

        //return the created news
        return response()->json(['error' => false, 'data' => $news, 'messages' => [ \Lang::get('news.storeSuccess', ['title' => $news->title])] ]);
    }

    /**
    *   @api {put} /api/news/:id used for update a news
    *   @apiName updateNews
    *   @apiGroup News
    *   @apiParam {String} title updated news title
    *   @apiParam {String} content updated content HTML available
    *   @apiParam {Boolean} draft updated not publish (draft)
    *   @apiParam {Boolean} star new is star
    *   @apiParam {String} picture base64 picture encoding
    *   @apiParam {DateTime} publish_at Y-m-d G:i:s for publish date
    *   @apiParam {Boolean} confirm news check by the corrector
    *   @apiParam {Number[]} tags news tags
    */
    public function updateById(Request $request, $id) 
    {
        $news = News::find($id);
        return $this->update($news, $request);
    }

    /**
    * @api {put} /api/news/:slug used for update a news by slug
    * @apiName updateNewsBySlug
    * @apiGroup News
    *   @apiParam {String} title updated news title
    *   @apiParam {String} content updated content HTML available
    *   @apiParam {Boolean} draft updated not publish (draft)
    *   @apiParam {Boolean} star new is star
    *   @apiParam {String} picture base64 picture encoding
    *   @apiParam {DateTime} publish_at Y-m-d G:i:s for publish date
    *   @apiParam {Boolean} confirm news check by the corrector
    *   @apiParam {Number[]} tags news tags
    */
    public function updateBySlug(Request $request, $slug)
    {
        $news = News::where('slug', '=', $slug)->first();
        return $this->update($news, $request);
    }

    /**
    *   @api {delete} /api/news/:id used for destroy a specific news - delete picture too
    *   @apiName destroyNews
    *   @apiGroup News
    *   @apiParam {Number} id news id to destroy
    */
    public function destroyById($id) 
    {
        $news = News::find($id);
        return $this->destroy($news);
    }
    
   /**
    *   @api {delete} /api/news/:slug used for destroy a specific news - delete picture too
    *   @apiName destroyNewsBySlug
    *   @apiGroup News
    *   @apiParam {String} slug slug to destroy
    */
    public function destroyBySlug($slug)
    {
        $news = News::where('slug', '=', $slug)->first();
        return $this->destroy($news);
    }
    
    //base function for update news
    public function update(News $news, Request $request)
    {
        //check if news exist
        if(!$news)
            return response()->json(['error' => true, 'data' => null, 'messages' => \Lang::get('news.notFound')]);

        //overload slug param in all case
        $request->merge(['slug' => '']); //default no slug
        if($request->input('title')) //if we have title - slug should be update too
             $request->merge(['slug'  => str_slug($request->input('title'), '-')]); //append slug for validation

        //validator
        $validator = Validator::make($request->all(), $this->rule());
        if($validator->fails()) 
        {
            return response()->json(['error' => true, 'data' => null, 'messages' => $validator->messages()]);
        }

        //picture
        $picture = $request->input('picture');
        if($picture)
        {
            $resultPicture = $this->validatePicture($picture);
            if($resultPicture instanceof JsonResponse)
                return $resultPicture;
        }

        $oldPicturePath = public_path().DIRECTORY_SEPARATOR.$news->picture; //keep the old path

        //input content
        $title = $request->input('title');
        $content = $request->input('content');
        $draft = $request->input('draft');
        $star = $request->input('star');
        $publish_at = $request->input('publish_at');
        $confirm = $request->input('confirm');
        $tags = $request->input('tags');
        
        //update field
        if($title !== null) $news->title = $title;
        if($content !== null) $news->content = $content;
        if($draft !== null) $news->draft = $draft;
        if($star !== null) $news->star = $star;
        if($publish_at !== null) Carbon::createFromFormat('Y-m-d G:i:s', $publish_at);
        if($confirm !== null) $news->confirm = $confirm;
        
        //try to save update picture
        //if fail - news not save and return an error
        if($picture)
        {
            try 
            {
               $resultPicture->save(public_path().DIRECTORY_SEPARATOR.$news->picture, 100);
            } 
            catch (\Intervention\Image\Exception\NotWritableException $e) 
            {
                return response()->json(['error' => false, 'data' => null, 'messages' => \Lang::get('news.cantSavePicture')]);
            }
        }
       
        $news->save(); //all is ok we can update the news

         //tags relationship - can't fail because of validator
        if($request->input('tags'))  $news->tags()->sync($request->input('tags'));
        $news->load('tags');

        //picture not update but title/slug is - need to rename old picture
        if(!$picture && $title) 
            \File::move($oldPicturePath, public_path().DIRECTORY_SEPARATOR.$news->picture);

        //return the update news
        return response()->json(['error' => false, 'data' => $news, 'messages' => [ \Lang::get('news.updateSuccess', ['title' => $news->title])] ]);
    }

    // base function for destroy news
    public function destroy(News $news)
    {
        if($news)
        {
            $news->delete();
            \File::delete(public_path().DIRECTORY_SEPARATOR.$news->picture); //delete picture of the news
        }

        return response()->json(['error' => ($news) ? false : true, 'data' => $news, 'messages' => [($news) ? \Lang::get('news.destroySuccess', ['title' => $news->title]) : \Lang::get('news.notFound')] ]);
    }
}
