<?php

namespace App\Http\Controllers\News;

use Illuminate\Http\Request;

use Carbon\Carbon;
use Validator;
use App\Http\Requests;
use App\Http\Controllers\Controller;
use App\Models\Tag;

class TagController extends Controller
{
    /**
    *   @api {get} /api/tags/ [get all tag]
    *   @apiName allTag
    *   @apiGroup Tags 
    */
    public function all() 
    {
        return response()->json(['error' => false, 'data' => Tag::all(),  'messages' => null]);
    }

    /**
    *   @api {get} /api/tags/:id [get tag detail]
    *   @apiName indexTag
    *   @apiGroup tag
    *   @apiParam {Number} id tag id to show
    */
    public function index($id) 
    {
        $tag = Tag::find($id);
        return response()->json(['error' => ($tag) ? false : true, 'data' => $tag, 'messages' => [ ($tag) ? null : \Lang::get('tag.notFound') ] ]);
    }
    
    /**
    *   @api {post} /api/tags/ [used for create a tag]
    *   @apiName storeTag
    *   @apiGroup News
    *   @apiParam {String} name tag name
    */
    public function store(Request $request)
    {
        $validator = Validator::make($request->all(), [
            'name' => 'required|string|unique:tag,name',
        ]);
        
        if($validator->fails()) 
            return response()->json(['error' => true, 'data' => null, 'messages' => $validator->messages()]);
            
        $tag = new Tag;
        $tag->name = $request->input('name');
        $tag->save();
        
        //return created tag
        return response()->json(['error' => false, 'data' => $tag, 'messages' => [ \Lang::get('tag.storeSuccess', ['name' => $tag->name]) ]]);
    }

    /**
    *   @api {put} /api/tag/:id used for update a tag
    *   @apiName updateTag
    *   @apiGroup Tag
    *   @apiParam {String} name updated name
    */
    public function update(Request $request, $id) 
    {
        $tag = Tag::find($id); 
        if(!$tag)
            return response()->json(['error' => true, 'data' => null, 'messages' => [ \Lang::get('tag.notFound') ] ]);
    
        $validator = Validator::make($request->all(), [
            'name' => 'required|string|unique:tag,name',
        ]);
        
        if($validator->fails()) 
            return response()->json(['error' => true, 'data' => null, 'messages' => $validator->messages()]);
            
        $tag->name = $request->input('name');
        $tag->save();
        
        //return updated tag
        return response()->json(['error' => false, 'data' => $tag, 'messages' => null]);
    }

    /**
    *   @api {delete} /api/tag/:id used for destroy specific tag
    *   @apiName destroyTag
    *   @apiGroup News
    *   @apiParam {Number} id  destroy tag id - remove the tag of news
    */
    public function destroy($id)
    {
        $tag = Tag::find($id);
        
        if($tag) 
            $tag->delete();
        
        return response()->json(['error' => ($tag) ? false : true, 'data' => $tag, 'messages' => ($tag) ? \Lang::get('tag.destroySucess', [ 'name' => $tag->name ]) : \Lang::get('tag.notFound') ]);
    }
}
