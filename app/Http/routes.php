<?php

/*
|--------------------------------------------------------------------------
| Routes File
|--------------------------------------------------------------------------
|
| Here is where you will register all of the routes in an application.
| It's a breeze. Simply tell Laravel the URIs it should respond to
| and give it the controller to call when that URI is requested.
|
*/

Route::group(['prefix' => 'api', 'as' => 'api::'], function () {
    
    /*
    |--------------------------------------------------------------------------
    | News Routes
    |--------------------------------------------------------------------------
    */
    Route::group(['prefix' => 'news', 'as' => 'news::'], function () {
        Route::get('/', ['as' => 'all', 'uses' => 'News\NewsController@all']); //used for display all news - filter and paginate
        Route::get('/{id}', ['as' => 'index', 'uses' => 'News\NewsController@index'])->where('id', '[0-9]+'); //used for display one news
        Route::get('/{slug}', ['as' => 'indexBySlug', 'uses' => 'News\NewsController@indexBySlug']); //used for display one news by slug
        Route::put('/{id}', ['as' => 'update', 'uses' => 'News\NewsController@updateById'])->where('id', '[0-9]+'); //used for editing and publish
        Route::put('/{slug}', ['as' => 'update', 'uses' => 'News\NewsController@updateBySlug']); //used for editing and publish news by slug
        Route::post('/', ['as' => 'store', 'uses' => 'News\NewsController@store']); //used for create news
        Route::delete('/{id}', ['as' => 'destroy', 'uses' => 'News\NewsController@destroyById'])->where('id', '[0-9]+'); //used for delete news
        Route::delete('/{slug}', ['as' => 'destroyBySlug', 'uses' => 'News\NewsController@destroyBySlug']); //used for delete news by slug
    });
    
   /*
    |--------------------------------------------------------------------------
    | Tag Routes
    |--------------------------------------------------------------------------
    */
    Route::group(['prefix' => 'tags', 'as' => 'tag::'], function () {
        Route::get('/', ['as' => 'all', 'uses' => 'News\TagController@all']); //used for display all tag
        Route::get('/{id}', ['as' => 'index', 'uses' => 'News\TagController@index']); //used for get one tag
        Route::put('/{id}', ['as' => 'update', 'uses' => 'News\TagController@update']); //used for update tag
        Route::post('/', ['as' => 'create', 'uses' => 'News\TagController@store']); //used for create a new one
        Route::delete('/{id}', ['as' => 'destroy', 'uses' => 'News\TagController@destroy']); //used for delete tag
    });
    
    /*
     |--------------------------------------------------------------------------
     | SSO Routes
     |-------------------------------------------------------------------------
     */
     
     Route::group(['prefix' => 'sso', 'as' => 'sso::'], function() {
        Route::get('/discourse', ['as' => 'discourse', 'uses' => 'Auth\AuthController@discourse']);
        Route::get('/mushraider', ['as' => 'mushraider', 'uses' => 'Auth\AuthController@mushraider']);
     });
});


/*
|--------------------------------------------------------------------------
| Application Routes
|--------------------------------------------------------------------------
|
| This route group applies the "web" middleware group to every route
| it contains. The "web" middleware group is defined in your HTTP
| kernel and includes session state, CSRF protection, and more.
|
*/

Route::group(['middleware' => ['web']], function () {
    //
});
