<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class News extends Model
{
    protected $table = 'news';
    protected $appends = array('picture');
    
    public function tags()
    {
        return $this->belongsToMany('App\Models\Tag');
    }
    
    public function getPictureAttribute()
    {
        return (string) 'img'.DIRECTORY_SEPARATOR.'news'.DIRECTORY_SEPARATOR.$this->slug.'.jpg';
    }
}
