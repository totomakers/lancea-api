# Lancea-api
### Simple api for gaming association LanceA
----

# Setup

#### Should have

+ Git
+ Composer

#### Do

+ run `composer install`
+ copy `.env.example` and rename to `.env`
+ make datatabse setup inside `.env`
+ run `php artisan migrate`
+ run `php artisan key:generate`

---

For running test server : `php artisan serv`
 
For production :

+ make a symlink to `public folder`
    + Windows : `mklink /d "{/path/to/link-name}" "{/path/folder/linked}"`
    + Linux : `ln -s {/path/to/file-name} {link-name}`