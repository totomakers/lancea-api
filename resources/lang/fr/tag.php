<?php

return [
    'notFound' => 'Ce tag est introuvable.',
    'storeSuccess' => 'Nouveau tag <:name> sauvegardé.',
    'updateSuccess' => 'Le tag <:name> a été mis à jour.',
    'destroySuccess' => 'Le tag <:name> a été supprimé.',
];