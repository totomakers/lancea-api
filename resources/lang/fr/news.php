<?php

return [
    'notFound' => 'Cette actualité  est introuvable.',
    'storeSuccess' => 'Nouvelle actualité <:title> sauvegardée.',
    'updateSuccess' => 'L\'actualité <:title> a été mis à jour.',
    'destroySuccess' => 'L\'actualité <:title> a été supprimé avec succèes.',
    'cantSavePicture' => 'Impossible de sauvegarder l\'image dans le répertoire',
    'badBase64' => 'La base64 de l\'image est erronée.',
    'tooHeavy' => 'L\'image est trop lourde et ne doit pas excéder :bytes bytes.',
];