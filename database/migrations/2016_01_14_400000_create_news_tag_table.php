<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateNewsTagTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        // create base schema
        Schema::create('news_tag', function (Blueprint $table) {
            $table->increments('id', true);
            $table->integer('news_id')->unsigned();
            $table->integer('tag_id')->unsigned();
            $table->unique(array('news_id', 'tag_id'));
        });
        
        // create foreign key
        Schema::table('news_tag', function (Blueprint $table) {
            $table->foreign('news_id')->references('id')->on('news')->onDelete('cascade');
            $table->foreign('tag_id')->references('id')->on('tag')->onDelete('cascade');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::drop('news_tag');
    }
}
