<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateNewsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        // create base schema
        Schema::create('news', function (Blueprint $table) {
            $table->increments('id', true);
            $table->string('slug', 60)->unique();
            $table->string('title', 50);
            $table->longText('content');
            $table->boolean('draft')->default(true);
            $table->boolean('confirm')->default(false);
            $table->boolean('star')->default(false);
            $table->timestamp('publish_at')->useCurrent();
            $table->integer('created_by')->unsigned()->nullable();
            $table->timestamps();
        });
        
        // create foreign key
        Schema::table('news', function (Blueprint $table) {
            $table->foreign('created_by')->references('id')->on('account')->onDelete('set null');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::drop('news');
    }
}
