<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateAccountTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        // create base schema
        Schema::create('account', function (Blueprint $table) {
            $table->increments('id', true);
            $table->string('username')->unique();
            $table->string('email')->unique(); //use gravatar for avatar <3
            $table->string('sha1_password', 20);
            $table->integer('rank')->unsigned();
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::drop('account');
    }
}
